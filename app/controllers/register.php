<?php

class Register extends Controller{

    public function index(){
        $data['judul'] = 'Register';
        $this->view('template/header', $data);
        $this->view('register/index');
        $this->view('template/footer');
    }

    public function prosesRegister(){
        if ($this->model('User_model')->register($_POST) > 0 ) {
            header('location: '. BASEURL . '/login');
        }
    }
}