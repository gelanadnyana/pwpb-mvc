<?php

class home extends Controller{

    public function __construct(){
        if(!isset($_SESSION["login"])){
            header("location: ". BASEURL . "/login");
        }
    }
    public function index()
    {
        $data['judul'] = 'home';
        $data['nama'] = $this->model('User_model')->getUser();
        $this->view('template/header', $data);
        $this->view('home/index', $data);
        $this->view('template/footer');
    }
}