<?php

class mahasiswa extends Controller{
    public function index()
    {
        $data['judul'] = 'Daftar Mahasiswa';
        $data['mhs'] = $this->model('mahasiswa_model')->getAllMahasiswa();
        $this->view('template/header', $data);
        $this->view('mahasiswa/index',$data);
        $this->view('template/footer');
    }

    public function coba(){
        echo "hai";
    }

}