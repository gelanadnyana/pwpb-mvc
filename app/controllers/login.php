<?php


class login extends Controller {

    public function __construct(){
        if(isset($_SESSION["login"])){
            header("location: ". BASEURL . "/home");
        }
    }

    public function index(){
        $data['judul'] = 'login';
        $this->view('template/header', $data);
        $this->view('login/index');
        $this->view('template/footer');
    }
    public function auth(){
        if($this->model('User_model')->login($_POST) > 0){
            header('location: '. BASEURL . '/ ');
        }else{

        }

    }
    public function logout(){
        session_unset();
        session_destroy();
        header('location: '. BASEURL . '/login');
        exit;
    }
}