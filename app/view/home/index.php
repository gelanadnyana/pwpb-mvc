<div class="container" ">
    <div class="container-fluid row mt-5">
        <div class="col-6">
        <img class="ms-5" src="<?= BASEURL;?>/image/texthome.png" alt="" width="400" >
        </div>
        <div class="col-6">
        <img class="" src="<?= BASEURL;?>/image/logohome.png" alt="" width="500">
        </div>
    </div>

    <!-- section 2 -->
    <!-- Bagian  -->
    <div class="row mt-5 fw-medium">
       <div class="text">
        <h4 class="mt-4">Recent Blog Post</h4>
       </div>
       <div class="col-6">
            <img src="<?= BASEURL; ?>/image/bg1.png" alt="" width="550">
                <p class="mt-3" style="color: #6941C6;">Olivia Rhye. <?php echo date("l m y");?></p>
                <h4>UX review presentations</h4>
            <p style="font-family: Inter;">How do you create compelling presentations that wow your colleagues and impress your managers?</p>
        <div class="d-flex">
            <div class="">
                <div class="border border-info badge rounded-pill text-info">Design</div>
            </div>
            <div class="ms-2">
                <div class="border border-success badge rounded-pill text-success">Research</div>
            </div>
            <div class="ms-2">
                <div class="border border-danger badge rounded-pill text-danger">Presentations</div>
            </div>
        </div>
       </div>
       <!-- bagian kiri -->
        <div class="col-6">
            <div class="row">
                <div class="col-7">
                    <img src="<?= BASEURL; ?>/image/bg2.png" alt="" >
                </div>
                <div class="col-4">
                    <p class="fw-bold">Migrating to Linear 101</p>
                        <p style="font-family: Inter;">Linear helps streamline software projects, sprints, tasks, and bug tracking.</p>
                        <div class="border border-info badge rounded-pill text-info">Read More</div>
                    <p style="color: #6941C6;">Phoenix Baker. <?php echo date("j F y");?></p>
                </div>
            </div>
        <div class="row">
            <div class="col-7">
              <img class="mt-3" src="<?= BASEURL; ?>/image/bg3.png" alt="">
            </div>
            <div class="col-4 mt-2">
                <p class="fw-bold">Migrating to Linear 101</p>
                    <p style="font-family: Inter;">Linear helps streamline software projects, sprints, tasks, and bug tracking.</p>
                    <div class="border border-info badge rounded-pill text-info">Read More</div>
                <p style="color: #6941C6;">Lana Steiner. <?php echo date("j F y");?></p>
            </div>
         </div>
      </div>
      </div>
      <div class="row mt-5">
        <div class="col-xl-6">
            <img src="<?= BASEURL;?>/image/bg4.png" alt="" width="550">
        </div>
        <div class="col-xl-6">
            <p style="color: #6941C6;">Olivia Rhye. <?php echo date("j F y");?></p>
            <p class="fw-bold">Grid system for better Design User Interface</p>
            <p>A grid system is a design tool used to arrange content on a webpage. It is a series of vertical and horizontal lines that create a matrix of intersecting points, which can be used to align and organize page elements. Grid systems are used to create a consistent look and feel across a website, and can help to make the layout more visually appealing and easier to navigate.</p>
            <div class="d-flex">
                <div class="">
                    <div class="border border-info badge rounded-pill text-info">Design</div>
                </div>
            <div class="ms-2">
                <div class="border border-danger badge rounded-pill text-danger">Interface</div>
            </div>
        </div>
        </div>
    </div>

    <!-- Section 3 -->
    <div class="row">
        <h4 class="mt-4">All blog post</h4>
        <div class="col-xl-4 mt-4">
            <img src="<?= BASEURL; ?>/image/bg5.png" alt="" width="350">
            <p class="mt-3" style="color: #6941C6;">Alec Whitten .<?php echo date("j F y");?></p>
            <h5>Bill Walsh leadership lessons</h5>
            <p>Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?</p>
            <div class="d-flex">
                <div class="">
                    <div class="border border-info badge rounded-pill text-info">Leadership</div>
                </div>
            <div class="ms-2">
                <div class="border border-secondary badge rounded-pill text-secondary">Management</div>
            </div>
        </div>
        
    </div>
    <div class="col-xl-4 mt-4">
        <img src="<?= BASEURL; ?>/image/bg6.png" alt="" width="350">
            <p class="mt-3" style="color: #6941C6;">Alec Whitten .<?php echo date("j F y");?></p>
            <h5>PM mental models</h5>
            <p>Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?</p>
            <div class="d-flex">
                <div class="">
                    <div class="border border-warning badge rounded-pill text-warning">Product</div>
                </div>
            <div class="ms-2">
                <div class="border border-info badge rounded-pill text-info">Research</div>
            </div>
        </div>
    </div>
    <div class="col-xl-4 mt-4">
        <img src="<?= BASEURL; ?>/image/bg7.png" alt="" width="350">
            <p class="mt-3" style="color: #6941C6;">Alec Whitten .<?php echo date("j F y");?></p>
            <h5>What is Wireframing?</h5>
            <p>Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?</p>
            <div class="d-flex">
                <div class="">
                    <div class="border border-danger badge rounded-pill text-danger">Design</div>
                </div>
            <div class="ms-2">
                <div class="border border-secondary badge rounded-pill text-secondary">Rsearch</div>
            </div>
</div>
</div>