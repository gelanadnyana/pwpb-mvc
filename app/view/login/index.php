<div class="container">
  <div class="row">
    <div class="col-xl-6">
      <img src="<?= BASEURL;?>/image/logohome.png" alt="" width="500">
    </div>
    <div class="col-md-6">
    <div class="container d-flex justify-content-center">
      <div class="rounded bg-light p-5 shadow" style="width: 500px; height: 400px;">
      <div class="header d-flex justify-content-center">
        <h1 class="text-dark">LOGIN</h1>
      </div>
        <form class="mt-4" action="<?= BASEURL; ?>/login/auth" method="POST">
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Username</label>
          <input type="text" name="username" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukan username anda">
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Password</label>
          <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Masukan password anda">
        </div>
        <div class="submit d-flex justify-content-center ">
        <button type="submit" class="btn btn-primary bg-dark border border-dark">Login</button>
        </div>
        <div class="register d-flex justify-content-center mt-3">
        <a href="<?= BASEURL;?>/register" class="text-decoration-none text-success  ">Register</a>
     </div>
    </form>
  </div>
</div>
    </div>
  </div>
</div>