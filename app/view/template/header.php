<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Halaman <?= $data['judul']; ?></title>
    <link rel="stylesheet" href="<?= BASEURL; ?>/css/bootstrap.css">
</head>
<body >

<nav class="navbar navbar-expand-lg p-5">
  <div class="container d-flex justify-content">
   <div class="judul">
   <a class="navbar-brand fw-bold fs-2" href=" <?= BASEURL; ?> ">Sport News</a>
   </div>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
      <div class="navbar-nav mx-auto">
        <a class="nav-link active" aria-current="page" href="<?= BASEURL; ?>">Blog</a>
        <a class="nav-link active" aria-current="page" href="<?= BASEURL; ?>">Category</a>
        <a class="nav-link active" aria-current="page" href="<?= BASEURL; ?>">Trending News</a>
        <a class="nav-link active" href="<?= BASEURL; ?>/about">About</a>
        <a class="nav-link active" href="<?= BASEURL; ?>/contact">Contact</a>
    </div>
    <div class="btn navbar-button bg-dark" style="">
    <?php if(isset($_SESSION['login'])) : ?>
      <a href="<?= BASEURL; ?>/login/logout" class="text-decoration-none fw-bold text-light">Logout</a>
      <?php elseif(!isset($_SESSION['login'])) : ?>
        <a href="<?= BASEURL; ?>/login" class="text-decoration-none fw-bold text-light">Login</a>
        <?php endif; ?>
    </div>
  </div>
</nav> 