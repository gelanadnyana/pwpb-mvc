<div class="container">
   <div class="row">
    <div class="col-xl-6 mt-5">
        <img src="<?= BASEURL; ?>/image/logohome.png" alt="" width="500">
    </div>
    <div class="col-xl-6">
    <div class="row d-flex justify-content-center">
        <div class="col-md-8 my-5 bg-light rounded p-5 shadow" style="width: 600px;">
            <form action="<?= BASEURL;?>/register/prosesRegister" method="POST">
            <div class="header d-flex justify-content-center">
                <h2 class="text-dark">REGISTER</h2>
            </div>
                <div class="mb-3">
                    <label for="exampleInputUser1" class="form-label">Username</label>
                    <input type="text" name="username" class="form-control" id="exampleInputEmail1"
                        aria-describedby="emailHelp" placeholder="Masukan username anda">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email</label>
                    <input type="text" name="email" class="form-control" id="exampleInputEmail1"
                        aria-describedby="emailHelp" placeholder="Masukan email anda">
                   
                </div>
                <div class="password row">
                    <div class="mb-3 col-6">
                        <label for="exampleInputPassword1" class="form-label">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Masukan password anda">
                    </div>
                    <div class="mb-3 col-6">
                        <label for="exampleInputPassword2" class="form-label">Confirm Password</label>
                        <input type="password" name="password2" class="form-control" id="exampleInputPassword1" placeholder="Konfirmasi password">
                    </div>
                </div>
                <div class="login d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-dark" >Register</button>
                </div>
            </form>
        </div>
    </div>
    </div>
   </div>
</div>