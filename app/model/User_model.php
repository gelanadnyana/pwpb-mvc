<?php

class User_model{
    private $table = 'user';
    private $db;

    public function __construct()
    {
        $this->db = new database;
    }   

    public static function getUser(){
        
    }
    public function register($data){
        $username = $data['username'];
        $password = $data['password'];
        $email = $data['email'];
        $cpassword = $data['password2'];

        if($password !== $cpassword){
            echo "password sing patuh";
        }
        $passwordHash = password_hash($password, PASSWORD_DEFAULT);
        $query = 'INSERT INTO user (username,password,email) VALUES (:username,:password,:email)';
        $this->db->query($query);
        $this->db->bind('username', $username);
        $this->db->bind('password', $passwordHash);
        $this->db->bind('email', $email);
        $this->db->execute();

        return 1;
    }

    public function login($data){
        $passwordPost = $data["password"];
        $query = "SELECT * FROM user WHERE username = :username";
        $this->db->query($query); 
        $this->db->bind("username", $data["username"]); 
        $user = $this->db->single();
        $passwordDb = $user["password"];
        if(password_verify($passwordPost, $passwordDb)){
            $_SESSION['login'] = true;
            return 1;
        }else{
            $_SESSION['login'] = false;
            return -1;
        }
    }
}